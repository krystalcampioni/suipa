$(function() {

  $("#scrolldown").click(function() {
      $('html, body').animate({
          scrollTop: $("#main").offset().top
      }, 2000);
  });

  $(".banner__scroll").click(function() {
      $('html, body').animate({
          scrollTop: $(".adoption-gallery").offset().top
      }, 2000);
  });

});
